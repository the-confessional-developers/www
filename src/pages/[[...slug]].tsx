import { allContents, Content } from "contentlayer/generated";
import { useMDXComponent } from "next-contentlayer/hooks";

export async function getStaticPaths() {
  const paths = allContents.map((content: Content) => content.url);
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }: any) {
  const content: Content | any = allContents.find((content: Content) =>
    params.slug.includes(content.slug)
  );
  if (content === undefined) {
    // StackOverflow tells me this will make NextJS return a 404
    return {
      notFound: true,
    };
  } else {
    return {
      props: {
        content,
      },
    };
  }
}

const PageLayout = ({ content }: { content: Content }) => {
  const MDXContent = useMDXComponent(content.body.code);
  return (
    <>
      <article>
        <MDXContent />
      </article>
    </>
  );
};

export default PageLayout;
