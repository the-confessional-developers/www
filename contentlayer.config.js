import { defineDocumentType, makeSource } from "contentlayer/source-files";

export const Content = defineDocumentType(() => ({
  name: "Content",
  filePathPattern: `**/*.mdx`,
  contentType: "mdx",
  fields: {
    title: {
      type: "string",
      description: "The title of the content",
      required: true,
    },
    date: {
      type: "date",
      description: "The date of the content",
      required: true,
    },
  },
  computedFields: {
    url: {
      type: "string",
      resolve: (x) => `/${x._raw.flattenedPath}`,
    },
    slug: {
      type: "string",
      resolve: (x) => x._raw.flattenedPath,
    },
  },
}));

export default makeSource({
  contentDirPath: "content",
  documentTypes: [Content],
});
